import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('www.google.com')

WebUI.setText(findTestObject('Object Repository/Page_Google/input_Iniciar sesin_q'), 'wiki')

WebUI.click(findTestObject('Object Repository/Page_Google/b_pedia'))

WebUI.navigateToUrl('https://www.google.com/search?source=hp&ei=oqKEXtDeBsS_5OUP_5qu8Ak&q=wikipedia&oq=wiki&gs_lcp=CgZwc3ktYWIQARgAMgUIABCDATICCAAyAggAMgIIADIFCAAQgwEyBQgAEIMBMgIIADIFCAAQgwEyBQgAEIMBMgUIABCDAVDyLFi2OWC_QGgAcAB4AIABX4gBpgOSAQE1mAEAoAEBqgEHZ3dzLXdperABAA&sclient=psy-ab')

WebUI.click(findTestObject('Object Repository/Page_wikipedia - Buscar con Google/h3_Wikipedia'))

WebUI.setText(findTestObject('Object Repository/Page_Wikipedia la enciclopedia libre/input_Buscar_search'), 'testeo')

WebUI.click(findTestObject('Object Repository/Page_Wikipedia la enciclopedia libre/input_Buscar_go'))

WebUI.navigateToUrl('https://es.wikipedia.org/wiki/Testeo')

WebUI.closeBrowser()

